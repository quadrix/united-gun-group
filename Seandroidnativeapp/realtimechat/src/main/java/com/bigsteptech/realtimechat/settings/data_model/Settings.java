package com.bigsteptech.realtimechat.settings.data_model;


public class Settings {

    String settingName, settingTitle;
    int onlineStatus, notificationsEnabled;

    public Settings(String settingName, String settingTitle, int onlineStatus, int notificationEnabled) {
        this.settingName = settingName;
        this.settingTitle = settingTitle;
        this.onlineStatus = onlineStatus;
        this.notificationsEnabled = notificationEnabled;
    }

    public String getSettingName() {
        return settingName;
    }

    public String getSettingTitle() {
        return settingTitle;
    }

    public int getOnlineStatus() {
        return onlineStatus;
    }

    public int isNotificationsEnabled() {
        return notificationsEnabled;
    }
}
