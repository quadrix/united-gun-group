
package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;

import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnCancelClickListener;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.PredicateLayout;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;
import java.util.Map;


public class FormBlockedUsers extends FormWidget implements OnCancelClickListener{

    protected SelectableTextView _textView, _descriptionTextView;
    protected String fieldName;
    FormWidget widget;
    LinearLayout.LayoutParams defaultLayoutParams;
    Context mContext;
    PredicateLayout mSelectedFriendsLayout;
    AppConstant mAppConst;
    private Map<String, String> postParams;
    private AlertDialogWithAction mAlertDialogWithAction;


    public FormBlockedUsers( final Context context, String property, String label,
                             boolean hasValidator, String description){
        super(context, property, hasValidator);
        fieldName = property;
        mContext = context;
        mAppConst = new AppConstant(mContext);
        postParams = new HashMap<>();
        CustomViews.setmOnCancelClickListener(this);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);

        //Set Label
        _textView = new SelectableTextView( context );
        _textView.setText(label);
        _textView.setTypeface(Typeface.DEFAULT_BOLD);
        _layout.addView(_textView);

        //Set description about blocked users
        if(description != null && !description.isEmpty()){
            _descriptionTextView = new SelectableTextView(context);
            _descriptionTextView.setText(description);

            _layout.addView(_descriptionTextView);
        }

    }


    @Override
    public String getValue(){

        return _textView.getText().toString();
    }

    @Override
    public void setValue( String value ) {

        if(value != null && !value.isEmpty()) {

            // Create layout for show blocked users list
            mSelectedFriendsLayout = new PredicateLayout(mContext);

            defaultLayoutParams = CustomViews.getWrapLayoutParams();
            int marginTop = (int) (mContext.getResources().getDimension(R.dimen.margin_10dp) /
                    mContext.getResources().getDisplayMetrics().density);
            defaultLayoutParams.setMargins(marginTop, marginTop, marginTop, marginTop);

            mSelectedFriendsLayout.setLayoutParams(defaultLayoutParams);

            try {
                Object json = new JSONTokener(value).nextValue();

                JSONArray valuesArray = (JSONArray) json;
                for(int i = 0; i < valuesArray.length(); i++){
                    JSONObject jsonObject = valuesArray.getJSONObject(i);
                    final int user_id = jsonObject.getInt("user_id");
                    String display_name = jsonObject.getString("displayname");

                    CustomViews.createSelectedUserLayout(mContext, user_id, display_name,
                            mSelectedFriendsLayout, null, 1);
                }

                _layout.addView(mSelectedFriendsLayout);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    public void setErrorMessage(String errorMessage){
        _textView.setError(errorMessage);
    }


    @Override
    public void onCancelButtonClicked(int userId) {

        final View canceledView = mSelectedFriendsLayout.findViewById(userId);
        postParams.put("user_id", String.valueOf(userId));

        mAlertDialogWithAction.showAlertDialogWithAction(mContext.getResources().getString(R.string.unblock_member_title),
                mContext.getResources().getString(R.string.unblock_member_message),
                mContext.getResources().getString(R.string.unblock_member_button),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        mAppConst.postJsonResponseForUrl(AppConstant.DEFAULT_URL + "block/remove", postParams,
                                new OnResponseListener() {
                                    @Override
                                    public void onTaskCompleted(JSONObject jsonObject) {
                                        SnackbarUtils.displaySnackbarLongTime(_layout,
                                                mContext.getResources().getString(R.string.unblock_member_success_message));
                                        _layout.removeView(mSelectedFriendsLayout);
                                        mSelectedFriendsLayout.removeView(canceledView);
                                        _layout.addView(mSelectedFriendsLayout);
                                    }

                                    @Override
                                    public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                        SnackbarUtils.displaySnackbarLongTime(_layout,
                                                message);
                                    }
                        });
                    }
                });

    }
}
