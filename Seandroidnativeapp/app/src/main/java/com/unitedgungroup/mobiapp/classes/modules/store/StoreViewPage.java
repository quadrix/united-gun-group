package com.unitedgungroup.mobiapp.classes.modules.store;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.activities.EditEntry;
import com.unitedgungroup.mobiapp.classes.common.activities.InviteGuest;
import com.unitedgungroup.mobiapp.classes.common.activities.ReportEntry;
import com.unitedgungroup.mobiapp.classes.common.adapters.FragmentAdapter;
import com.unitedgungroup.mobiapp.classes.common.formgenerator.FormActivity;
import com.unitedgungroup.mobiapp.classes.common.ui.BadgeView;
import com.unitedgungroup.mobiapp.classes.common.ui.BaseButton;
import com.unitedgungroup.mobiapp.classes.common.ui.CircularImageView;
import com.unitedgungroup.mobiapp.classes.common.ui.SlidingTabLayout;
import com.unitedgungroup.mobiapp.classes.common.utils.LogUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SocialShareUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.modules.album.AlbumUtil;
import com.unitedgungroup.mobiapp.classes.modules.offers.BrowseOffersFragment;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoLightBoxActivity;
import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoListDetails;
import com.unitedgungroup.mobiapp.classes.modules.store.fragments.BrowseProductFragment;
import com.unitedgungroup.mobiapp.classes.modules.store.fragments.ReviewFragment;
import com.unitedgungroup.mobiapp.classes.modules.store.fragments.StoreInfoFragment;
import com.unitedgungroup.mobiapp.classes.modules.user.profile.userProfile;
import com.unitedgungroup.mobiapp.classes.modules.video.VideoUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class StoreViewPage extends FormActivity implements ViewPager.OnPageChangeListener, View.OnClickListener,
        AppBarLayout.OnOffsetChangedListener {
    private AppConstant mAppConst;
    private SocialShareUtil mSocialShareUtil;
    private ViewPager mPager;
    private FragmentAdapter mPagerAdapter;
    private ImageView mStoreCover;
    private BaseButton mLikeButton;
    private BadgeView mCartCountBadge;
    private CircularImageView mOwnerPic;
    private TextView mStoreTitleView, mOwnerTitle, mLikeCountView;
    private JSONObject mStoreDetails,mInfoTabObject,mProfileInfoObject;
    private JSONArray mProfileTabsArray,mGutterMenusArray;
    private SlidingTabLayout slidingTabLayout;
    private ProgressBar mProgressBar;
    private String mMenuFieldName,mMenuFieldValue, mStoreUrl,mOverViewData,mStoreTitle;
    private Map<String, String> postParams;
    private ArrayList<Object> mCoverImageDetails;
    private int isLiked,isStoreClosed,userId,storeId,mLikeCount;
    boolean isProductTab = false;
    private String mStoreViewPageUrl;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private TextView mToolBarTitle;
    private Toolbar mToolbar;
    FloatingActionButton mFilterButton;
    private int siteVideoPluginEnabled, mAdvVideosCount;
    private BottomSheetBehavior mBottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_view_page);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolBarTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolBarTitle.setSelected(true);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getResources().getString(R.string.blank_string));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        final View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setHideable(true);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);


        mAppConst = new AppConstant(this);
        mSocialShareUtil = new SocialShareUtil(this);
        postParams = new HashMap<>();
        mCoverImageDetails = new ArrayList<>();

        mFilterButton = (FloatingActionButton) findViewById(R.id.filter_fab);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mOwnerPic = (CircularImageView) findViewById(R.id.owner_image);
        mOwnerTitle = (TextView) findViewById(R.id.owner_title);
        mStoreCover = (ImageView) findViewById(R.id.cover_image);
        mStoreTitleView = (TextView) findViewById(R.id.store_title);
        mLikeCountView = (TextView) findViewById(R.id.like_count);
        mLikeButton = (BaseButton) findViewById(R.id.like_button);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mPager = (ViewPager) findViewById(R.id.pager);

        mFilterButton.setVisibility(View.GONE);
        mPagerAdapter = new FragmentAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setCustomTabView(R.layout.tab_indicator, android.R.id.text1);
        slidingTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this,R.color.white));
        slidingTabLayout.setDistributeEvenly(true);

        // When the page is selected, change the page details
        slidingTabLayout.setOnPageChangeListener(this);
        mLikeButton.setOnClickListener(this);
        mOwnerPic.setOnClickListener(this);
        ((AppBarLayout) findViewById(R.id.appbar)).addOnOffsetChangedListener(this);

        mStoreViewPageUrl = UrlUtil.STORE_VIEW_URL + getIntent().getStringExtra("store_id")+ "?cart=1";
        getStoreDetails();


    }

    public void getStoreDetails(){
        mAppConst.getJsonResponseFromUrl(mStoreViewPageUrl, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) throws JSONException {
                mStoreDetails = jsonObject;
                checkSiteVideoPluginEnabled();
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                mProgressBar.setVisibility(View.GONE);
                SnackbarUtils.displaySnackbar(slidingTabLayout, message);
            }
        });
    }

    /**
     *  This calling will return sitevideoPluginEnabled to 1 if
     *  1. Adv Video plugin is integrated with Directory/Pages plugin
     *  2. And if there is any video uploaded in this page using Avd video
     *  else it will return sitevideoPluginEnabled to 0
     */
    public void checkSiteVideoPluginEnabled(){

        String url = UrlUtil.IS_SITEVIDEO_ENABLED + "?subject_id=" + getIntent().getStringExtra("store_id") +
                "&subject_type=sitestore_store";
        mAppConst.getJsonResponseFromUrl(url, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                siteVideoPluginEnabled = jsonObject.optInt("sitevideoPluginEnabled");
                mAdvVideosCount = jsonObject.optInt("totalItemCount");
                setStoreDetails();
            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                setStoreDetails();
            }
        });

    }

    private void setStoreDetails(){
        userId = mStoreDetails.optInt("owner_id");
        storeId = mStoreDetails.optInt("store_id");
        mStoreTitle = mStoreDetails.optString("title");
        mStoreTitleView.setText(mStoreTitle);
        mToolBarTitle.setText(mStoreTitle);
        mOwnerTitle.setText(mStoreDetails.optString("owner_title"));
        mGutterMenusArray = mStoreDetails.optJSONArray("gutterMenu");
        mStoreUrl = mStoreDetails.optString("content_url");
        isStoreClosed = mStoreDetails.optInt("closed");
        invalidateOptionsMenu();
        Picasso.with(this)
                .load(mStoreDetails.optString("owner_image_profile"))
                .into(mOwnerPic);

        Picasso.with(this)
                .load(mStoreDetails.optString("image"))
                .into(mStoreCover);

        mCoverImageDetails.add(new PhotoListDetails(mStoreDetails.optString("image")));

        mStoreCover.setOnClickListener(this);

        mInfoTabObject = mStoreDetails.optJSONObject("basic_information");
        mProfileInfoObject = mStoreDetails.optJSONObject("profile_information");
        mOverViewData = mStoreDetails.optString("overview");
        mProfileTabsArray =  mStoreDetails.optJSONArray("profileTabs");

        // Add Videos tab if siteVideo plugin is integrated with Store plugin
        if(siteVideoPluginEnabled == 1){

            JSONObject singleJsonObject= new JSONObject();
            String tabUrl = "advancedvideos/index?subject_id=" + storeId + "&subject_type=sitestore_store";
            try {
                singleJsonObject.put("totalItemCount", mAdvVideosCount);
                singleJsonObject.put("name", "videos");
                singleJsonObject.put("label", getResources().getString(R.string.action_bar_title_video));
                singleJsonObject.put("url", tabUrl);
                mProfileTabsArray.put(singleJsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        for(int i = 0; i < mProfileTabsArray.length(); i++){
            JSONObject tabsObject =  mProfileTabsArray.optJSONObject(i);
            String tabName =  tabsObject.optString("name");
            Fragment fragment = getTabFragment(tabName,tabsObject.optString("url"),tabsObject.optJSONObject("urlParams"));
            if(fragment != null){
                LogUtils.LOGD("StoreView","Tab Object - "+tabsObject.toString());
                if(tabsObject.has("count")) {
                    mPagerAdapter.addFragment(fragment, tabsObject.optString("label")
                            + " ("+tabsObject.optInt("count")+") ");
                }else {
                    mPagerAdapter.addFragment(fragment, tabsObject.optString("label"));
                }
            }
            mPagerAdapter.notifyDataSetChanged();
            slidingTabLayout.setViewPager(mPager);
        }
        mPager.setOffscreenPageLimit(mPagerAdapter.getCount() + 1);
        isLiked =  mStoreDetails.optInt("is_liked");
        mLikeCount = mStoreDetails.optInt("like_count");
        mLikeCountView.setVisibility(View.VISIBLE);
        mLikeCountView.setText(String.valueOf(mLikeCount));
        if(!mAppConst.isLoggedOutUser()){
            mLikeButton.setVisibility(View.VISIBLE);
            if(isLiked == 1){
                mLikeButton.setActivated(false);
                mLikeButton.setText(getResources().getString(R.string.unlike));
            }else {
                mLikeButton.setActivated(true);
                mLikeButton.setText(getResources().getString(R.string.like_text));
            }
        }else {
            mLikeButton.setVisibility(View.GONE);
        }
    }

    private Fragment getTabFragment(String tabName, String tabUrl, JSONObject urlParams){
        Bundle bundle = new Bundle();
        bundle.putString(ConstantVariables.EXTRA_MODULE_TYPE,ConstantVariables.STORE_MENU_TITLE);
        bundle.putInt(ConstantVariables.VIEW_PAGE_ID,storeId);
        Fragment fragment = null;
        switch (tabName){
            case "products":
                fragment = BrowseProductFragment.newInstance(null,null);
                bundle.putInt("store_id",storeId);
                fragment.setArguments(bundle);
                mFilterButton.setVisibility(View.VISIBLE);
                isProductTab = true;
                break;
            case "update":
                // TODO - Will release in store plugin second version
                break;
            case "information":
                fragment = new StoreInfoFragment();
                bundle.putString(ConstantVariables.RESPONSE_OBJECT,mInfoTabObject.toString());
                if(mProfileInfoObject != null && mProfileInfoObject.length() > 0) {
                    bundle.putString("profileInfo",mProfileInfoObject.toString());
                }
                bundle.putString(ConstantVariables.OVERVIEW,mOverViewData);
                break;
            case "photos":
                fragment = AlbumUtil.getBrowsePageInstance();
                break;
            case "reviews":
                fragment = new ReviewFragment();
                fragment.setArguments(bundle);
                break;
            case "coupons":
                fragment = new BrowseOffersFragment();
                fragment.setArguments(bundle);
                break;
            case "video":
            case "videos":
                if (siteVideoPluginEnabled == 0) {
                    String viewPageUrl = AppConstant.DEFAULT_URL
                            + "videogeneral/view?subject_type=sitestorevideo_video&subject_id=";
                    tabUrl += "?subject_type="+urlParams.optString("subject_type")
                            +"&subject_id="+ urlParams.optInt("subject_id");
                    bundle.putString(ConstantVariables.VIEW_PAGE_URL, viewPageUrl);
                    bundle.putString(ConstantVariables.VIDEO_SUBJECT_TYPE, ConstantVariables.SITE_STORE_VIDEO_MENU_TITLE);
                }
                fragment = VideoUtil.getBrowsePageInstance();
                bundle.putInt(ConstantVariables.ADV_VIDEO_INTEGRATED, siteVideoPluginEnabled);
                bundle.putBoolean("isProfilePageRequest", true);
                break;

        }
        if(fragment != null){
            bundle.putString(ConstantVariables.URL_STRING, AppConstant.DEFAULT_URL + tabUrl);
            fragment.setArguments(bundle);
        }
        return fragment;
    }
    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if(verticalOffset == -collapsingToolbarLayout.getHeight() + mToolbar.getHeight()){
            mToolBarTitle.setVisibility(View.VISIBLE);
        }else{
            mToolBarTitle.setVisibility(View.GONE);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.default_menu_item, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        menu.clear();

        // Not showing the option menu if the gutter menus null.
        if(mGutterMenusArray != null){
            for(int i = 0; i < mGutterMenusArray.length(); i++){
                try {
                    JSONObject menuJsonObject = mGutterMenusArray.optJSONObject(i);
                    switch (menuJsonObject.getString("name")) {
                        case "share":
                            menu.add(Menu.NONE, i, Menu.FIRST, menuJsonObject.getString("label").trim())
                                    .setIcon(R.drawable.ic_share_white)
                                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            break;
                        case "cart":
                            menu.add(Menu.NONE, i, Menu.FIRST, menuJsonObject.getString("label").trim())
                                    .setIcon(R.drawable.ic_shopping_cart_24dp)
                                    .setActionView(R.layout.cart_menu)
                                    .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                            break;
                        case "close":
                        case "open":
                            if (isStoreClosed == 0) {
                                menu.add(Menu.NONE, i, Menu.NONE, getString(R.string.close_store));
                            } else {
                                menu.add(Menu.NONE, i, Menu.NONE, getString(R.string.open_store));
                            }
                            break;
                        default:
                            menu.add(Menu.NONE, i, Menu.NONE, menuJsonObject.getString("label").trim());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            mCartCountBadge = (BadgeView) menu.getItem(mGutterMenusArray.length()-1)
                    .getActionView().findViewById(R.id.cart_item_count);
            menu.getItem(mGutterMenusArray.length()-1)
                    .getActionView().setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(StoreViewPage.this, CartView.class);
                            startActivityForResult(intent,ConstantVariables.VIEW_PAGE_CODE);
                        }
                    });
            if(mCartCountBadge != null && !PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("0") &&
                    !PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("")
                    && !PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT).equals("null")){
                mCartCountBadge.setVisibility(View.VISIBLE);
                mCartCountBadge.setText(PreferencesUtils.getNotificationsCounts(this,PreferencesUtils.CART_COUNT));
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        JSONObject menuJsonObject, urlParams;
        String menuName, redirectUrl;
        //noinspection SimplifiableIfStatement

        if (id == android.R.id.home) {
            onBackPressed();
            // Playing backSound effect when user tapped on back button from tool bar.
            if (PreferencesUtils.isSoundEffectEnabled(this)) {
                SoundUtil.playSoundEffectOnBackPressed(this);
            }
        } else if(mGutterMenusArray != null) {
            postParams.clear();
            menuJsonObject = mGutterMenusArray.optJSONObject(id);
            menuName = menuJsonObject.optString("name");
            urlParams = menuJsonObject.optJSONObject("urlParams");
            redirectUrl = AppConstant.DEFAULT_URL + menuJsonObject.optString("url");
            if (urlParams != null && urlParams.length() != 0) {
                JSONArray urlParamsNames = urlParams.names();
                for (int j = 0; j < urlParams.length(); j++) {
                    mMenuFieldName = urlParamsNames.optString(j);
                    mMenuFieldValue = urlParams.optString(mMenuFieldName);
                    postParams.put(mMenuFieldName, mMenuFieldValue);
                    redirectUrl = mAppConst.buildQueryString(redirectUrl,postParams);

                }
            }
            switch (menuName) {
                case "invite":
                    Intent invite = new Intent(this, InviteGuest.class);
                    invite.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.STORE_MENU_TITLE);
                    invite.putExtra(ConstantVariables.URL_STRING, redirectUrl);
                    startActivity(invite);
                    break;
                case "share":
                    mSocialShareUtil.sharePost(findViewById(item.getItemId()), mStoreDetails.optString("title"),
                            mStoreDetails.optString("image"),
                            mAppConst.buildQueryString(redirectUrl, postParams),null, mStoreUrl);
                    break;

                case "report":
                    Intent reportEntry = new Intent(this, ReportEntry.class);
                    reportEntry.putExtra(ConstantVariables.URL_STRING, redirectUrl);
                    startActivity(reportEntry);
                    break;

                case "open":
                case "close":
                    if(isStoreClosed == 1) {
                        performAction(redirectUrl,
                                getResources().getString(R.string.open_store_msg),
                                getResources().getString(R.string.open_store_dialogue_title),
                                getResources().getString(R.string.yes_label),
                                getResources().getString(R.string.open_store_success), menuName);

                    }else {
                        performAction(redirectUrl,
                                getResources().getString(R.string.close_store_msg),
                                getResources().getString(R.string.close_store_dialogue_title),
                                getResources().getString(R.string.yes_label),
                                getResources().getString(R.string.close_store_success), menuName);
                    }
                    break;
                case "tellafriend":
                    Intent intent = new Intent(this, EditEntry.class);
                    intent.putExtra(ConstantVariables.URL_STRING, redirectUrl);
                    intent.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.STORE_MENU_TITLE);
                    intent.putExtra(ConstantVariables.FORM_TYPE, "tellafriend");
                    intent.putExtra(ConstantVariables.CONTENT_TITLE, menuJsonObject.optString("label"));
                    startActivityForResult(intent, ConstantVariables.CREATE_REQUEST_CODE);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    break;
                case "delete":
                    performAction(redirectUrl,
                            getResources().getString(R.string.delete_dialogue_message),
                    getResources().getString(R.string.delete_dialogue_title),
                    getResources().getString(R.string.delete_dialogue_button),
                    getResources().getString(R.string.delete_dialogue_success_message),menuName);
                    break;
                case "create_review":
                    Intent reviewIntent = new Intent(this, CreateNewEntry.class);
                    reviewIntent.putExtra(ConstantVariables.CREATE_URL, redirectUrl);
                    reviewIntent.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.STORE_MENU_TITLE);
                    reviewIntent.putExtra(ConstantVariables.FORM_TYPE, "create_review");
                    startActivityForResult(reviewIntent, ConstantVariables.CREATE_REQUEST_CODE);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    break;

                case "update_review":
                    Intent updateReviewIntent = new Intent(this, EditEntry.class);
                    updateReviewIntent.putExtra(ConstantVariables.URL_STRING, redirectUrl);
                    updateReviewIntent.putExtra(ConstantVariables.EXTRA_MODULE_TYPE, ConstantVariables.STORE_MENU_TITLE);
                    updateReviewIntent.putExtra(ConstantVariables.FORM_TYPE, "update_review");
                    startActivityForResult(updateReviewIntent, ConstantVariables.CREATE_REQUEST_CODE);
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    break;

            }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

        if(position == 0 && isProductTab){
            mFilterButton.show();
        }else {
            mFilterButton.hide();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.like_button:
                likeButtonAction();
                break;
            case R.id.owner_image:
                Intent intent = new Intent(this, userProfile.class);
                intent.putExtra(ConstantVariables.USER_ID,userId);
                startActivityForResult(intent,ConstantVariables.VIEW_PAGE_CODE);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;
            case R.id.cover_image:
                Bundle bundle = new Bundle();
                bundle.putSerializable(PhotoLightBoxActivity.EXTRA_IMAGE_URL_LIST, mCoverImageDetails);
                Intent i = new Intent(StoreViewPage.this, PhotoLightBoxActivity.class);
                i.putExtra(ConstantVariables.TOTAL_ITEM_COUNT, 1);
                i.putExtra(ConstantVariables.SHOW_OPTIONS, false);
                i.putExtras(bundle);
                startActivityForResult(i, ConstantVariables.VIEW_LIGHT_BOX);
                break;
        }

    }
    String mLikeUnlikeUrl;
    public void likeButtonAction(){

        mAppConst.showProgressDialog();
        Map<String, String> likeParams = new HashMap<>();
        likeParams.put(ConstantVariables.SUBJECT_TYPE, "sitestore_store");
        likeParams.put(ConstantVariables.SUBJECT_ID, String.valueOf(storeId));

        mLikeUnlikeUrl = AppConstant.DEFAULT_URL + (isLiked == 0 ? "like":"unlike");

        mAppConst.postJsonResponseForUrl(mLikeUnlikeUrl, likeParams, new OnResponseListener() {
            @Override
            public void onTaskCompleted(JSONObject jsonObject) throws JSONException {
                mAppConst.hideProgressDialog();
                if(isLiked == 0) {
                    mLikeCount+=1;
                    isLiked = 1;
                    mLikeButton.setText(getResources().getString(R.string.unlike));
                    mLikeButton.setActivated(false);
                    mLikeCountView.setText(String.valueOf(mLikeCount));
                }else {
                    mLikeCount-=1;
                    isLiked = 0;
                    mLikeButton.setText(getResources().getString(R.string.like_text));
                    mLikeButton.setActivated(true);
                    mLikeCountView.setText(String.valueOf(mLikeCount));
                }

            }

            @Override
            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                mAppConst.hideProgressDialog();
                SnackbarUtils.displaySnackbar(slidingTabLayout, message);
            }

        });
    }
        /*
    Code For Performing Action According to Name
     */

    public void performAction(final String url, final String message, String title, String buttonTitle,
                              final String showSuccessMessage, final String selectedMenuName){

        try {

            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);

            alertBuilder.setMessage(message);
            alertBuilder.setTitle(title);

            alertBuilder.setPositiveButton(buttonTitle, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                    if (selectedMenuName.equals("delete")) {
                        mAppConst.showProgressDialog();
                        mAppConst.deleteResponseForUrl(url, postParams, new OnResponseListener() {
                            @Override
                            public void onTaskCompleted(JSONObject jsonObject) {

                                mAppConst.hideProgressDialog();
                            /* Show Message */
                                SnackbarUtils.displaySnackbar(slidingTabLayout, showSuccessMessage);
                                onBackPressed();
                            }

                            @Override
                            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                mAppConst.hideProgressDialog();
                                SnackbarUtils.displaySnackbar(slidingTabLayout, message);
                            }
                        });
                    } else {
                        mAppConst.postJsonResponseForUrl(url, postParams, new OnResponseListener() {
                            @Override
                            public void onTaskCompleted(JSONObject jsonObject) {
                            /* Show Message */
                                if (selectedMenuName.equals("close")) {
                                    isStoreClosed = isStoreClosed == 0 ? 1 :0;
                                }
                                SnackbarUtils.displaySnackbar(slidingTabLayout, showSuccessMessage);
                            }

                            @Override
                            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                SnackbarUtils.displaySnackbar(slidingTabLayout, message);
                            }
                        });
                    }
                }
            });

            alertBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertBuilder.create().show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Updating the cart count
        if(mCartCountBadge != null) {
            if (!PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT).equals("0") &&
                    !PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT).equals("")
                    && !PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT).equals("null")) {
                mCartCountBadge.setVisibility(View.VISIBLE);
                mCartCountBadge.setText(PreferencesUtils.getNotificationsCounts(this, PreferencesUtils.CART_COUNT));
            } else{
                mCartCountBadge.setVisibility(View.GONE);
            }
        }

    }
}
