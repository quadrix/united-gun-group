/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.core;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.common.utils.DataStorage;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.startscreens.HomeScreen;

import org.json.JSONException;
import org.json.JSONObject;


public class WelcomeScreen extends AppCompatActivity {

    private AppConstant mAppConst;
    private Context mContext;
    private Bundle mBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome_screen);
        mContext = this;

        mAppConst = new AppConstant(mContext);

        TextView mLoadingText = (TextView) findViewById(R.id.loading_text);
        mLoadingText.setText(mContext.getResources().getString(R.string.loading_text) + "…");

        if (getIntent().getExtras() != null && getIntent().hasExtra("previousSelected")) {
            mBundle = getIntent().getExtras();

            // clearing the dashboard and cache data for loading the new language data.
            DataStorage.clearApplicationData(mContext);
            PreferencesUtils.updateDashBoardData(mContext,
                    PreferencesUtils.DASHBOARD_MENUS,
                    null);
            mLoadingText.setText(mContext.getResources().getString(R.string.language_update) + "...");
        }


        /* Logged-in user */

        if ((PreferencesUtils.getAuthToken(WelcomeScreen.this) != null &&
                !PreferencesUtils.getAuthToken(WelcomeScreen.this).isEmpty()) || mBundle != null) {

            mLoadingText.setVisibility(View.VISIBLE);
            mAppConst.refreshUserData();

            new Handler().postDelayed(new Runnable() {

                public void run() {
                    Intent intent;
                    if (!mAppConst.isLoggedOutUser()) {
                        intent = new Intent(WelcomeScreen.this, MainActivity.class);
                    } else {
                        intent = new Intent(WelcomeScreen.this, HomeScreen.class);
                    }
                    if (mBundle != null) {
                        intent.putExtras(mBundle);
                    }
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }, 500);

        } else {
            new Handler().postDelayed(new Runnable() {

                public void run() {

                    mAppConst.getJsonResponseFromUrl(UrlUtil.DASHBOARD_URL + "?browse_as_guest=1",
                            new OnResponseListener() {
                                @Override
                                public void onTaskCompleted(JSONObject jsonObject) throws JSONException {

                                    if (jsonObject != null) {
                                        PreferencesUtils.updateGuestUserSettings(mContext, jsonObject.optString("browse_as_guest"));
                                        PreferencesUtils.updateLocationEnabledSetting(mContext, jsonObject.optInt("location"));
                                        JSONObject mLanguageObject = jsonObject.optJSONObject("languages");
                                        if (mLanguageObject != null && mLanguageObject.length() != 0) {
                                            String mDefaultLanguageCode = mLanguageObject.optString("default");
                                            mAppConst.changeAppLocale(mDefaultLanguageCode, false);
                                        }

                                        mAppConst.saveDashboardValues(jsonObject);
                                        Intent mainIntent = new Intent(WelcomeScreen.this, HomeScreen.class);
                                        startActivity(mainIntent);
                                        finish();
                                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                    }

                                }

                                @Override
                                public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                    Intent mainIntent = new Intent(WelcomeScreen.this, HomeScreen.class);
                                    startActivity(mainIntent);
                                    finish();
                                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                }
                            });


                }
            }, 2000);
        }

    }

}
