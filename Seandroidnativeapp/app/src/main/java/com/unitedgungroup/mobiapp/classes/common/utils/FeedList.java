/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.common.utils;

import android.os.Parcel;
import android.os.Parcelable;

import com.unitedgungroup.mobiapp.classes.modules.photoLightBox.PhotoListDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FeedList implements Parcelable{

    private String mFeedIcon, mMenuUrl, mHashTagString;
    private String mFeedTitle, mFeedPostTime, mWebUrl, mActionTypeBody, mFeedAttachmentType;
    private int mAttachmentCount, mLikeCount, mCommentCount, mIsLike, mPhotoAttachmentCount;
    private int mCommentAble, mShareAble, mIsSaveFeedOption;
    private int mCanComment;
    private JSONArray mFeedMenusArray, mFeedAttachmentArray, mFeedFilterArray;
    private JSONObject mFeedFooterMenus, mFeedPostMenus,mFeedObject, mMyFeedReactions;
    String mHiddenBodyText, mUndoHiddenFeedURl, mHideAllText, mHideAllUrl, mHideAllName, mFeedType, mObjectType;
    int mSubjectId, mActionId, mObjectId, mOwnerFeedType;
    HashMap<String, String> mClickableStringsList, mClickableStringsListNew;
    HashMap<Integer, String> mVideoInfo;
    HashMap<String, String> mUndoHiddenFeedParams;
    boolean noFeed;
    ArrayList<PhotoListDetails> mPhotoDetails;
    double mLatitude, mLongitude;
    String mPlaceId, mLocationLabel;
    int mReactionsEnabled;
    JSONObject mFeedReactions;
    JSONObject mReactions;
    JSONArray mUserTagArray;

    public FeedList(int actionId, int subjectId, int feedType, String objectType, int objectId,
                    String mFeedTitle, String mFeedIcon, JSONArray mFeedMenusArray, String mFeedPostTime,
                    int attachmentCount, int likeCount, int commentCount, int canComment, int isLike,
                    JSONObject feedObject, JSONArray feedAttachmentArray, int photoAttachmentCount,
                    JSONObject feedFooterMenus, int commentAble, int shareAble,
                    int isSaveFeedOption, HashMap<String, String> clickableStrings, HashMap<String, String> clickableStringsNew,
                    String actionTypeBody, HashMap<Integer, String> videoInfo, String url, String feedAttachmentType, String type,
                    String locationLabel, double latitude, double longitude, String placeId, String hashTagString,
                    JSONObject feedReactions, JSONObject myFeedReactions, JSONArray userTagArray) {

        this.mSubjectId = subjectId;
        mOwnerFeedType = feedType;
        mObjectType = objectType;
        mObjectId = objectId;
        this.mFeedTitle = mFeedTitle;
        this.mFeedIcon = mFeedIcon;
        this.mFeedMenusArray = mFeedMenusArray;
        this.mFeedPostTime = mFeedPostTime;
        mAttachmentCount = attachmentCount;
        mLikeCount = likeCount;
        mCommentCount = commentCount;
        mIsLike = isLike;
        mCanComment = canComment;
        this.mFeedObject = feedObject;
        this.mFeedAttachmentArray = feedAttachmentArray;
        this.mPhotoAttachmentCount = photoAttachmentCount;
        this.mCommentAble = commentAble;
        this.mShareAble = shareAble;
        this.mIsSaveFeedOption = isSaveFeedOption;
        mFeedFooterMenus = feedFooterMenus;
        mActionId = actionId;
        mClickableStringsList = clickableStrings;
        mClickableStringsListNew = clickableStringsNew;
        mVideoInfo = videoInfo;
        mWebUrl = url;
        mActionTypeBody = actionTypeBody;
        mFeedAttachmentType = feedAttachmentType;
        mFeedType = type;
        mLatitude = latitude;
        mLongitude = longitude;
        mPlaceId = placeId;
        mLocationLabel = locationLabel;
        mHashTagString = hashTagString;
        mFeedReactions = feedReactions;
        mMyFeedReactions = myFeedReactions;
        mUserTagArray = userTagArray;
    }

    public FeedList(JSONObject feedPostMenus, JSONArray FeedFilterArray, boolean isNoFeed, int reactionEnable,
                    JSONObject reactions){
        mFeedPostMenus =  feedPostMenus;
        mFeedFilterArray = FeedFilterArray;
        this.noFeed = isNoFeed;
        mReactionsEnabled = reactionEnable;
        mReactions = reactions;
    }

    public FeedList(int likeCount) {
        mLikeCount = likeCount;
    }

    public boolean isNoFeed() {
        return noFeed;
    }

    public String getmFeedIcon() {
        return mFeedIcon;
    }

    public String getmFeedTitle() {
        return mFeedTitle;
    }

    public String getmFeedPostTime() {
        return mFeedPostTime;
    }

    public JSONArray getmFeedMenusArray() {
        return mFeedMenusArray;
    }

    public void setmFeedMenusArray(JSONArray mFeedMenusArray) {
        this.mFeedMenusArray = mFeedMenusArray;
    }

    public String getHashTagString() {
        return mHashTagString;
    }

    public int getmAttachmentCount() {
        return mAttachmentCount;
    }

    public int getmLikeCount() {
        return mLikeCount;
    }

    public void setmLikeCount(int mLikeCount) {
        this.mLikeCount = mLikeCount;
    }

    public int getmCommentCount() {
        return mCommentCount;
    }

    public void setmCommentCount(int mCommentCount) {
        this.mCommentCount = mCommentCount;
    }

    public int getmIsLike() {
        return mIsLike;
    }

    public void setmIsLike(int mIsLike) {
        this.mIsLike = mIsLike;
    }

    public int ismCanComment() {
        return mCanComment;
    }

    public void setmCanComment(int mCanComment) {
        this.mCanComment = mCanComment;
    }

    public int getmPhotoAttachmentCount() {
        return mPhotoAttachmentCount;
    }

    public JSONArray getmFeedAttachmentArray() {
        return mFeedAttachmentArray;
    }

    public JSONObject getmFeedFooterMenus() {
        return mFeedFooterMenus;
    }

    public void setmFeedFooterMenus(JSONObject mFeedFooterMenus) {
        this.mFeedFooterMenus = mFeedFooterMenus;
    }

    public String getmHiddenBodyText() {
        return mHiddenBodyText;
    }

    public void setmHiddenBodyText(String mHiddenBodyText) {
        this.mHiddenBodyText = mHiddenBodyText;
    }

    public String getmUndoHiddenFeedURl() {
        return mUndoHiddenFeedURl;
    }

    public void setmUndoHiddenFeedURl(String mUndoHiddenFeedURl) {
        this.mUndoHiddenFeedURl = mUndoHiddenFeedURl;
    }

    public HashMap<String, String> getmUndoHiddenFeedParams() {
        return mUndoHiddenFeedParams;
    }

    public void setmUndoHiddenFeedParams(HashMap<String, String> mUndoHiddenFeedParams) {

        this.mUndoHiddenFeedParams = mUndoHiddenFeedParams;
    }

    public String getmHideAllText() {
        return mHideAllText;
    }

    public void setmHideAllText(String mHideAllText) {
        this.mHideAllText = mHideAllText;
    }

    public String getmHideAllUrl() {
        return mHideAllUrl;
    }

    public void setmHideAllUrl(String mHideAllUrl) {
        this.mHideAllUrl = mHideAllUrl;
    }

    public String getmHideAllName() {
        return mHideAllName;
    }

    public void setmHideAllName(String mHideAllName) {
        this.mHideAllName = mHideAllName;
    }

    public int getmCommentAble() {
        return mCommentAble;
    }

    public void setmCommentAble(int mCommentAble) {
        this.mCommentAble = mCommentAble;
    }

    public int getmShareAble() {
        return mShareAble;
    }

    public void setmShareAble(int mShareAble) {
        this.mShareAble = mShareAble;
    }

    public int getmIsSaveFeedOption() {
        return mIsSaveFeedOption;
    }

    public void setmIsSaveFeedOption(int mIsSaveFeedOption) {
        this.mIsSaveFeedOption = mIsSaveFeedOption;
    }

    public int getmSubjectId() {
        return mSubjectId;
    }

    public void setmSubjectId(int mSubjectId) {
        this.mSubjectId = mSubjectId;
    }

    public int getmActionId() {
        return mActionId;
    }

    public HashMap<String, String> getmClickableStringsList() {
        return mClickableStringsList;
    }

    public JSONArray getmFeedFilterArray() {
        return mFeedFilterArray;
    }

    public JSONObject getmFeedPostMenus() {
        return mFeedPostMenus;
    }

    public ArrayList<PhotoListDetails> getmPhotoDetails() {
        return mPhotoDetails;
    }

    public void setmPhotoDetails(ArrayList<PhotoListDetails> mPhotoDetails) {
        this.mPhotoDetails = mPhotoDetails;
    }

    public String getmActionTypeBody() {
        return mActionTypeBody;
    }

    public JSONObject getmFeedObject() {
        return mFeedObject;
    }

    public HashMap<Integer, String> getmVideoInfo() {
        return mVideoInfo;
    }

    public String getmWebUrl() {
        return mWebUrl;
    }

    public String getmFeedType() {
        return mFeedType;
    }

    public String getmFeedAttachmentType() {
        return mFeedAttachmentType;
    }

    protected FeedList(Parcel in) {
        mFeedIcon = in.readString();
        mMenuUrl = in.readString();
        mHashTagString = in.readString();
        mFeedTitle = in.readString();
        mFeedPostTime = in.readString();
        mWebUrl = in.readString();
        mActionTypeBody = in.readString();
        mFeedAttachmentType = in.readString();
        mAttachmentCount = in.readInt();
        mLikeCount = in.readInt();
        mCommentCount = in.readInt();
        mIsLike = in.readInt();
        mPhotoAttachmentCount = in.readInt();
        mCommentAble = in.readInt();
        mShareAble = in.readInt();
        mIsSaveFeedOption = in.readInt();
        mCanComment = in.readInt();
        try {
            mFeedMenusArray = in.readByte() == 0x00 ? null : new JSONArray(in.readString());
            mFeedAttachmentArray = in.readByte() == 0x00 ? null : new JSONArray(in.readString());
            mFeedFilterArray = in.readByte() == 0x00 ? null : new JSONArray(in.readString());
            mFeedFooterMenus = in.readByte() == 0x00 ? null : new JSONObject(in.readString());
            mFeedPostMenus = in.readByte() == 0x00 ? null : new JSONObject(in.readString());
            mFeedObject = in.readByte() == 0x00 ? null : new JSONObject(in.readString());
            mMyFeedReactions = in.readByte() == 0x00 ? null : new JSONObject(in.readString());
            mFeedReactions = in.readByte() == 0x00 ? null : new JSONObject(in.readString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mHiddenBodyText = in.readString();
        mUndoHiddenFeedURl = in.readString();
        mHideAllText = in.readString();
        mHideAllUrl = in.readString();
        mHideAllName = in.readString();
        mFeedType = in.readString();
        mObjectType = in.readString();
        mSubjectId = in.readInt();
        mActionId = in.readInt();
        mObjectId = in.readInt();
        mOwnerFeedType = in.readInt();
        mClickableStringsList = (HashMap) in.readValue(HashMap.class.getClassLoader());
        mVideoInfo = (HashMap) in.readValue(HashMap.class.getClassLoader());
        mUndoHiddenFeedParams = (HashMap) in.readValue(HashMap.class.getClassLoader());
        noFeed = in.readByte() != 0x00;
        if (in.readByte() == 0x01) {
            mPhotoDetails = new ArrayList<PhotoListDetails>();
            in.readList(mPhotoDetails, PhotoListDetails.class.getClassLoader());
        } else {
            mPhotoDetails = null;
        }
        mLatitude = in.readDouble();
        mLongitude = in.readDouble();
        mPlaceId = in.readString();
        mLocationLabel = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFeedIcon);
        dest.writeString(mMenuUrl);
        dest.writeString(mHashTagString);
        dest.writeString(mFeedTitle);
        dest.writeString(mFeedPostTime);
        dest.writeString(mWebUrl);
        dest.writeString(mActionTypeBody);
        dest.writeString(mFeedAttachmentType);
        dest.writeInt(mAttachmentCount);
        dest.writeInt(mLikeCount);
        dest.writeInt(mCommentCount);
        dest.writeInt(mIsLike);
        dest.writeInt(mPhotoAttachmentCount);
        dest.writeInt(mCommentAble);
        dest.writeInt(mShareAble);
        dest.writeInt(mIsSaveFeedOption);
        dest.writeInt(mCanComment);
        if (mFeedMenusArray == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mFeedMenusArray.toString());
        }
        if (mFeedAttachmentArray == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mFeedAttachmentArray.toString());
        }
        if (mFeedFilterArray == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mFeedFilterArray.toString());
        }
        if (mFeedFooterMenus == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mFeedFooterMenus.toString());
        }
        if (mFeedPostMenus == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mFeedPostMenus.toString());
        }
        if (mFeedObject == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mFeedObject.toString());
        }
        if (mMyFeedReactions == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mMyFeedReactions.toString());
        }
        if (mFeedReactions == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeString(mFeedReactions.toString());
        }
        dest.writeString(mHiddenBodyText);
        dest.writeString(mUndoHiddenFeedURl);
        dest.writeString(mHideAllText);
        dest.writeString(mHideAllUrl);
        dest.writeString(mHideAllName);
        dest.writeString(mFeedType);
        dest.writeString(mObjectType);
        dest.writeInt(mSubjectId);
        dest.writeInt(mActionId);
        dest.writeInt(mObjectId);
        dest.writeInt(mOwnerFeedType);
        dest.writeValue(mClickableStringsList);
        dest.writeValue(mVideoInfo);
        dest.writeValue(mUndoHiddenFeedParams);
        dest.writeByte((byte) (noFeed ? 0x01 : 0x00));
        if (mPhotoDetails == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(mPhotoDetails);
        }
        dest.writeDouble(mLatitude);
        dest.writeDouble(mLongitude);
        dest.writeString(mPlaceId);
        dest.writeString(mLocationLabel);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<FeedList> CREATOR = new Parcelable.Creator<FeedList>() {
        @Override
        public FeedList createFromParcel(Parcel in) {
            return new FeedList(in);
        }

        @Override
        public FeedList[] newArray(int size) {
            return new FeedList[size];
        }
    };

    public void setmFeedAttachmentArray(JSONArray mFeedAttachmentArray) {
        this.mFeedAttachmentArray = mFeedAttachmentArray;
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public String getmPlaceId() {
        return mPlaceId;
    }

    public String getmLocationLabel() {
        return mLocationLabel;
    }

    public String getmMenuUrl() {
        return mMenuUrl;
    }

    public void setmMenuUrl(String mMenuUrl) {
        this.mMenuUrl = mMenuUrl;
    }

    public void setmActionId(int mActionId) {
        this.mActionId = mActionId;
    }

    public void setmActionTypeBody(String mActionTypeBody) {
        this.mActionTypeBody = mActionTypeBody;
    }

    public int getmReactionsEnabled() {
        return mReactionsEnabled;
    }

    public JSONObject getmReactions() {
        return mReactions;
    }

    public int getmObjectId() {
        return mObjectId;
    }

    public String getmObjectType() {
        return mObjectType;
    }

    public int getmOwnerFeedType() {
        return mOwnerFeedType;

    }

    public JSONObject getmFeedReactions() {
        return mFeedReactions;
    }

    public JSONObject getmMyFeedReactions() {
        return mMyFeedReactions;
    }

    public void setmMyFeedReactions(JSONObject mMyFeedReactions) {
        this.mMyFeedReactions = mMyFeedReactions;
    }

    public HashMap<String, String> getmClickableStringsListNew() {
        return mClickableStringsListNew;
    }

    public void setmFeedReactions(JSONObject mFeedReactions) {
        this.mFeedReactions = mFeedReactions;
    }

    public JSONArray getmUserTagArray() {
        return mUserTagArray;
    }
}
