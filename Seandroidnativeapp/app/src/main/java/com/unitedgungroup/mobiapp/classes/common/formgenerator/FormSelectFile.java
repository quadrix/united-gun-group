package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import com.unitedgungroup.mobiapp.classes.common.activities.CreateNewEntry;
import com.unitedgungroup.mobiapp.classes.common.activities.EditEntry;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.multiimageselector.MultiImageSelectorActivity;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.core.ConstantVariables;

import android.widget.LinearLayout.LayoutParams;

public class FormSelectFile extends FormWidget {
    protected SelectableTextView _label;

    protected Context mContext;
    CreateNewEntry createNewEntry;
    EditEntry editEntry;
    View view;
    Button btnPhoto,btnMusic;

    private GridView mResultView;

    private int selectedMode;
    private String mCurrentSelectedOption;
    private boolean mIsApplyJobOption = false;


    // LogCat tag
    private static final String TAG = FormSelectFile.class.getSimpleName();



    public FormSelectFile(Context context, final String name, final boolean createForm,
                          String currentSelectedOption) {
        super(context, name, false);
        mCurrentSelectedOption = currentSelectedOption;
        mContext = context;
        createNewEntry = CreateNewEntry.getInstance();
        editEntry = EditEntry.getInstance();
        _label = new SelectableTextView( mContext );
        _label.setText(getDisplayText());
        _label.setLayoutParams(FormActivity.defaultLayoutParams);

        int margin = (int) (mContext.getResources().getDimension(R.dimen.margin_10dp) /
                mContext.getResources().getDisplayMetrics().density);

        LayoutParams params = CustomViews.getWrapLayoutParams();
        params.setMargins(margin, margin, margin, margin);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            params.setMarginEnd(margin);
            params.setMarginStart(margin);
        }

        LayoutInflater inflater = (LayoutInflater)  mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.select_file_layout, null);
        btnPhoto = (Button) view.findViewById(R.id.button);
        btnMusic = (Button) view.findViewById(R.id.buttonMusic);
        mResultView = (GridView)view.findViewById(R.id.result);

        if(mCurrentSelectedOption != null){
            switch (mCurrentSelectedOption)  {
                case "core_main_music":
                    if(name.equals("songs")){
                        btnMusic.setVisibility(View.VISIBLE);
                        btnPhoto.setVisibility(View.GONE);
                    }
                    btnPhoto.setText(mContext.getResources().getString(R.string.select_single_photo));
                    break;
                case "core_main_classified":
                case "core_main_event":
                case "core_main_group":
                case "core_main_forum":
                case "add_to_playlist":
                    btnPhoto.setText(mContext.getResources().getString(R.string.select_single_photo));
                    break;
                case "core_main_video":
                case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                    btnPhoto.setText(mContext.getResources().getString(R.string.select_video_btn));
                    break;

                case "core_main_siteevent":
                case "sitereview_listing":
                case "sitepage":
                case "core_main_sitegroup":
                case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                case ConstantVariables.ADV_VIDEO_PLAYLIST_MENU_TITLE:
                    if (name.equals("host_photo")) {
                        btnPhoto.setText(mContext.getResources().getString(R.string.host_photo_text));
                        mResultView.setId(R.id.host_image);
                    } else if (mCurrentSelectedOption.equals(ConstantVariables.MLT_MENU_TITLE)
                            && name.equals("filename")) {
                        mIsApplyJobOption = true;
                        btnPhoto.setText(mContext.getResources().getString(R.string.upload_file));
                    } else {
                        btnPhoto.setText(mContext.getResources().getString(R.string.select_main_photo));
                    }
                    break;

                default:
                    btnPhoto.setText(mContext.getResources().getString(R.string.go_select));
                    break;
            }

            if(!createForm) {
                switch (mCurrentSelectedOption) {
                    case "core_main_music":
                    case "core_main_classified":
                    case "core_main_forum":
                        view.setVisibility(View.VISIBLE);
                        break;
                }
            }
        }

        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentSelectedOption != null && createForm) {

                    switch (mCurrentSelectedOption) {
                        case "core_main_classified":
                        case "core_main_group":
                        case "core_main_music":
                        case "core_main_event":
                        case "core_main_forum":
                        case "sitereview_listing":
                        case "core_main_sitegroup":
                        case "core_main_siteevent":
                        case "add_to_playlist":
                        case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                        case ConstantVariables.ADV_VIDEO_CHANNEL_MENU_TITLE:
                        case ConstantVariables.ADV_VIDEO_PLAYLIST_MENU_TITLE:
                            selectedMode = MultiImageSelectorActivity.MODE_SINGLE;
                            break;
                        case "core_main_video":
                            selectedMode = MultiImageSelectorActivity.MODE_MULTI;
                            break;
                        default:
                            selectedMode = MultiImageSelectorActivity.MODE_MULTI;

                    }
                }

                switch (mCurrentSelectedOption) {
                    case "core_main_video":
                    case ConstantVariables.ADV_VIDEO_MENU_TITLE:
                        if(createForm){
                            createNewEntry.checkPermission(mContext, selectedMode, true, "video", null);
                        }
                        break;

                    case ConstantVariables.MLT_MENU_TITLE:
                        if (createForm) {
                            createNewEntry.checkPermission(mContext, selectedMode, true, "photo", name);
                        } else {
                            String type = mIsApplyJobOption ? "file": "photo";
                            editEntry.checkPermission(mContext, selectedMode, true, type, name);
                        }
                        break;

                    default:
                        if (createForm) {
                            createNewEntry.checkPermission(mContext, selectedMode, true, "photo", name);
                        } else {
                            editEntry.checkPermission(mContext, selectedMode, true, "photo", name);
                        }
                }
            }
        });

        btnMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (createForm) {
                    createNewEntry.checkPermission(mContext, selectedMode, true, "music", null);
                } else {
                    editEntry.checkPermission(mContext, selectedMode, true, "music", name);
                }
            }
        });


        _layout.addView(view);

    }
}