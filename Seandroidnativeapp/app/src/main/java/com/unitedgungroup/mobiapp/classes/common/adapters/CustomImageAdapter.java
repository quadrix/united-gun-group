/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.common.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnItemClickListener;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnUserLayoutClickListener;
import com.unitedgungroup.mobiapp.classes.common.ui.BezelImageView;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.viewholder.ProgressViewHolder;
import com.unitedgungroup.mobiapp.classes.common.utils.ImageViewList;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Provide views to RecyclerView with data from mDataSet.
 */
public class CustomImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int TYPE_HEADER = 0;
    public static final int TYPE_ITEM = 1;
    public static final int TYPE_PROGRESS = 2;

    private Activity mActivity;
    private Bundle mBundle;
    private String mSubjectType;
    List<ImageViewList> mPhotoList;
    private int mImageWidth;
    ImageViewList mImageViewList;
    private OnItemClickListener mOnItemClickListener;
    private OnUserLayoutClickListener mOnUserLayoutClickListener;
    ImageViewList mList;


    /**
     * Initialize the dataset of the Adapter.
     *
     * @param photoList containing the data to populate views to be used by RecyclerView.
     */
    public CustomImageAdapter(Activity activity, ImageViewList list, List<ImageViewList> photoList,
                              int columnWidth, Bundle bundle, String subjectType,
                              OnItemClickListener onItemClickListener,
                              OnUserLayoutClickListener onUserLayoutClickListener) {

        mOnItemClickListener = onItemClickListener;
        mOnUserLayoutClickListener = onUserLayoutClickListener;
        mActivity = activity;
        mPhotoList = photoList;
        mImageWidth = columnWidth;
        mBundle = bundle;
        mSubjectType = subjectType;
        mList = list;
    }

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param photoList containing the data to populate views to be used by RecyclerView.
     */
    public CustomImageAdapter(Activity activity, ImageViewList list, List<ImageViewList> photoList,
                              int columnWidth, OnItemClickListener onItemClickListener) {

        mOnItemClickListener = onItemClickListener;
        mList = list;
        mActivity = activity;
        mPhotoList = photoList;
        mImageWidth = columnWidth;
        mBundle = new Bundle();
        mSubjectType = "";
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {


        switch (viewType) {
            case TYPE_ITEM:
                //inflate your layout and pass it to view holder
                // Create a new view.
                View v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.pager_photo_view, viewGroup, false);
                return new ViewHolder(v, mImageWidth);

            case TYPE_HEADER:
                //inflate your layout and pass it to view holder
                View headerView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_imageview_header, viewGroup, false);
                return new HeaderViewHolder(headerView);
            default:
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                        R.layout.progress_item, viewGroup, false);

                return new ProgressViewHolder(view);
        }
    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)


    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        if (viewHolder instanceof ViewHolder) {
            mImageViewList = getItem(position);
            ((ViewHolder) viewHolder).holderImageList = mImageViewList;
            if (mImageViewList.getmGridViewImageUrl() != null && !mImageViewList.getmGridViewImageUrl().isEmpty()) {
                Picasso.with(mActivity.getApplicationContext())
                        .load(mImageViewList.getmGridViewImageUrl())
                        .placeholder(R.drawable.default_error)
                        .into(((ViewHolder) viewHolder).imageView);
            }
            ((ViewHolder) viewHolder).container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(v, position - 1);
                }
            });
        } else if (viewHolder instanceof HeaderViewHolder) {
            if (mList.getOwnerImageUrl() != null && !mList.getOwnerImageUrl().isEmpty()) {
                Picasso.with(mActivity.getApplicationContext())
                        .load(mList.getOwnerImageUrl())
                        .placeholder(R.drawable.default_user_profile)
                        .into(((HeaderViewHolder) viewHolder).mOwnerImageView);
            }

            ((HeaderViewHolder) viewHolder).mOwnerTitleView.setText(mList.getOwnerTitle());

            if (mList.getAlbumDescription() != null && mList.getAlbumDescription().length() > 0) {
                ((HeaderViewHolder) viewHolder).mViewDescription.setVisibility(View.VISIBLE);
                ((HeaderViewHolder) viewHolder).mViewDescription.setText(mList.getAlbumDescription());
            } else {
                ((HeaderViewHolder) viewHolder).mViewDescription.setVisibility(View.GONE);
            }
            ((HeaderViewHolder) viewHolder).mOwnerImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnUserLayoutClickListener != null) {
                        mOnUserLayoutClickListener.onUserLayoutClick();
                    }
                }
            });
            ((HeaderViewHolder) viewHolder).mOwnerTitleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mOnUserLayoutClickListener != null) {
                        mOnUserLayoutClickListener.onUserLayoutClick();
                    }
                }
            });
        } else {
            ProgressViewHolder.inflateProgressBar(((ProgressViewHolder) viewHolder).progressView);
        }
    }

    // END_INCLUDE(recyclerViewOnBindViewHolder)

    @Override
    public int getItemViewType(int position) {

        // Hiding header for the subject type = sitereview_album and for the profile/cover update.
        if (position < 1 && mBundle == null && !mSubjectType.equals("sitereview_album")) {
            return TYPE_HEADER;
        } else if (getItem(position) == null) {
            return TYPE_PROGRESS;
        } else {
            return TYPE_ITEM;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (mBundle == null && !mSubjectType.equals("sitereview_album")) {
            return mPhotoList.size() + 1;
        } else {
            return mPhotoList.size();
        }
    }

    private ImageViewList getItem(int position) {
        if (mBundle == null && !mSubjectType.equals("sitereview_album")) {
            return mPhotoList.get(position - 1);
        } else {
            return mPhotoList.get(position);
        }
    }
    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        public ImageViewList holderImageList;
        public View container;

        public ViewHolder(View v, int imageWidth) {
            super(v);

            container = v;
            imageView = (ImageView) v.findViewById(R.id.thumbnail);
            imageView.setLayoutParams(CustomViews.getCustomWidthHeightRelativeLayoutParams(imageWidth, imageWidth));


        }

    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    public static class HeaderViewHolder extends RecyclerView.ViewHolder {

        BezelImageView mOwnerImageView;
        private TextView mOwnerTitleView;
        private SelectableTextView mViewDescription;
        public View mUserView;

        public HeaderViewHolder(View v) {
            super(v);
            mUserView = v;
            mOwnerImageView = (BezelImageView) v.findViewById(R.id.owner_image);
            mOwnerTitleView = (TextView) v.findViewById(R.id.owner_title);
            mViewDescription = (SelectableTextView) v.findViewById(R.id.view_description);

        }
    }

}
