/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *    You may not use this file except in compliance with the
 *    SocialEngineAddOns License Agreement.
 *    You may obtain a copy of the License at:
 *    https://www.socialengineaddons.com/android-app-license
 *    The full copyright and license information is also mentioned
 *    in the LICENSE file that was distributed with this
 *    source code.
 */

package com.unitedgungroup.mobiapp.classes.modules.stickers;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;

import org.json.JSONObject;


public class StickersUtil {

    public static StickersPopup mStickersPopup;
    private static RelativeLayout mStickersParenView;
    private static AppConstant mAppConst;
    private static Context mContext;
    private static TextView mPhotoUploadingButton, mCommentPostButton;
    private static EditText mEditText;

    /**
     * Function to create a Emoji Popup
     * @param context Context of the activity
     * @param rootView View in which popup will be added
     * @param editText Reference of Edittext in which Emojis Will be shown when clicked.
     */

    public static StickersPopup createStickersPopup(Context context, View rootView, RelativeLayout stickersParentView,
                                                    final EditText editText,
                                                    JSONObject stickersResponse,
                                                    TextView photoUploadingButton, TextView commentPostButton){

        mContext = context;
        mPhotoUploadingButton = photoUploadingButton;
        mCommentPostButton = commentPostButton;
        mStickersParenView = stickersParentView;
        mAppConst = new AppConstant(mContext);
        mEditText = editText;

        // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
        mStickersPopup = new StickersPopup(rootView, mContext, stickersResponse, stickersParentView);

        //Will automatically set size according to the soft keyboard size
        mStickersPopup.setSizeForSoftKeyboard();

        // Hide the emoji popup on click of edit text
        mEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mStickersParenView.setVisibility(View.GONE);
                editText.requestFocus();
                mCommentPostButton.setTextColor(ContextCompat.getColor(mContext, R.color.gray_stroke_color));
                if(mPhotoUploadingButton != null){
                    mPhotoUploadingButton.setClickable(true);
                    mPhotoUploadingButton.setTextColor(ContextCompat.getColor(mContext, R.color.grey_dark));
                }
            }
        });

        mEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    mStickersParenView.setVisibility(View.GONE);
                    editText.requestFocus();
                    mCommentPostButton.setTextColor(ContextCompat.getColor(mContext, R.color.gray_stroke_color));
                    if(mPhotoUploadingButton != null){
                        mPhotoUploadingButton.setClickable(true);
                        mPhotoUploadingButton.setTextColor(ContextCompat.getColor(mContext, R.color.grey_dark));
                    }
                }
            }
        });

        return mStickersPopup;

    }

    /**
     * Show Emoji Keybaord when Emoji Icon will be clicked
     */
    public static void showStickersKeyboard(){

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        //If popup is not showing => sticker keyboard is not visible, we need to show it
        // Disable photo uploading button if stickers keyboard is visible

        if(mStickersParenView.getVisibility() == View.GONE){
            mEditText.clearFocus();
            mPhotoUploadingButton.setClickable(false);
            mPhotoUploadingButton.setTextColor(ContextCompat.getColor(mContext, R.color.gray_stroke_color));
            mCommentPostButton.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
            mStickersParenView.setVisibility(View.VISIBLE);
            mAppConst.hideKeyboard();
        } else {
            mEditText.requestFocus();
            mPhotoUploadingButton.setClickable(true);
            mPhotoUploadingButton.setTextColor(ContextCompat.getColor(mContext, R.color.grey_dark));
            mCommentPostButton.setTextColor(ContextCompat.getColor(mContext, R.color.gray_stroke_color));
            mStickersParenView.setVisibility(View.GONE);
        }

    }
}
