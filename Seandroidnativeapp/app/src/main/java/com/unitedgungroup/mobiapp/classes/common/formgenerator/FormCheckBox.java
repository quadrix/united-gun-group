package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.adapters.BrowseMemberAdapter;
import com.unitedgungroup.mobiapp.classes.common.dialogs.AlertDialogWithAction;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.core.AppConstant;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;

import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FormCheckBox extends FormWidget implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    protected AppCompatCheckBox _checkbox;
    Context mContext;
    RelativeLayout listLayout;
    TextView textView;
    AppConstant mAppConst;
    Map<String, String> postParams;
    ArrayList<FormWidget> widgets;

    AlertDialogWithAction mAlertDialogWithAction;

    public FormCheckBox(Context context, String property, String label, boolean hasValidator,
                        int defaultValue, ArrayList<FormWidget> _widget, String currentSelectedModule) {
        super( context, property, hasValidator );

        mContext = context;
        mAppConst = new AppConstant(mContext);
        mAlertDialogWithAction = new AlertDialogWithAction(mContext);
        postParams = new HashMap<>();
        widgets = _widget;

        if (!FormActivity.sIsAddToDiaryDescription) {
            if(property.contains("diary") || property.contains("Diary") || property.contains("wishlist")
                    || currentSelectedModule.equals("add_to_friend_list")
                    || property.contains("inplaylist")) {
                AppCompatTextView textView = new AppCompatTextView(context);
                int padding  = (int) mContext.getResources().getDimension(R.dimen.padding_6dp);
                textView.setPadding(0, 0, 0, padding);
                textView.setText(FormActivity.addToDiaryDescription);
                textView.setTextColor(ContextCompat.getColor(context, R.color.body_text_1));
                _layout.addView(textView);
                FormActivity.sIsAddToDiaryDescription = true;
            }
        }

        _checkbox = new AppCompatCheckBox( context );
        _checkbox.setText(label);

        switch (property){

            case "monthlyType":
                _checkbox.setId(R.id.monthly_type);
                _checkbox.setTag(property);
                _checkbox.setChecked(defaultValue != 0);
                break;
            case "host_link":
                _checkbox.setId(R.id.social_link);
                _checkbox.setTag(R.id.social_link);
                _checkbox.setOnCheckedChangeListener(this);
                break;
            case "isCopiedDetails":
                _checkbox.setTag("copy_purchaser_info");
                _checkbox.setOnCheckedChangeListener(this);
                break;
            default:
                _checkbox.setId(R.id.form_checkbox);
                break;
        }

        if (property.equals("host_link") && FormHostChange.sIsEditHost && defaultValue != 0) {
            _checkbox.setChecked(true);
        } else {
            _checkbox.setChecked(defaultValue != 0);
        }

        /**
         * Add Cross icon on Add To List page to delete Lists (Add to List which comes on Friends tab of
         * member profile page).
         */
        if (currentSelectedModule.equals("add_to_friend_list")) {

            listLayout = new RelativeLayout(context);
            listLayout.setId(R.id.property);

            RelativeLayout.LayoutParams layoutParams = CustomViews.getFullWidthRelativeLayoutParams();
            listLayout.setLayoutParams(layoutParams);

            textView = new TextView(context);
            RelativeLayout.LayoutParams textViewParams = CustomViews.getFullWidthRelativeLayoutParams();

            textViewParams.addRule(RelativeLayout.ALIGN_PARENT_END, R.id.property);
            textViewParams.addRule(RelativeLayout.RIGHT_OF, R.id.form_checkbox);

            int paddingLeft = (int) mContext.getResources().getDimension(R.dimen.padding_5dp);
            int paddingRight = (int) mContext.getResources().getDimension(R.dimen.padding_30dp);

            textView.setPadding(paddingLeft, paddingLeft, paddingRight, paddingLeft);

            textView.setLayoutParams(textViewParams);
            textView.setTag(property);
            textView.setGravity(Gravity.END);
            textView.setOnClickListener(this);

            Typeface fontIcon = GlobalFunctions.getFontIconTypeFace(mContext);
            textView.setTypeface(fontIcon);
            textView.setText("\uf00d");

            listLayout.setTag(property);

            listLayout.addView(_checkbox);
            listLayout.addView(textView);

            _layout.addView(listLayout);
            _layout.setTag(property);

        } else {
            _layout.addView( _checkbox );
        }


    }

    @Override
    public String getValue() {
        return String.valueOf( _checkbox.isChecked() ? "1" : "0" );
    }

    public void setValue( String value ) {
        _checkbox.setChecked( value.equals("1") );
        if (_checkbox.getId() == R.id.monthly_type) {
            _checkbox.setTag(value);
        }
    }

    @Override
    public void setErrorMessage(String errorMessage) {

        if(!_checkbox.isChecked()){
            _checkbox.setError(errorMessage);
        }
    }

    @Override
    public void setToggleHandler( FormActivity.FormWidgetToggleHandler handler ) {
        super.setToggleHandler(handler);
        _checkbox.setOnCheckedChangeListener( new ChangeHandler(this) );
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (buttonView.getId() == R.id.social_link && FormActivity.loadEditHostForm > 0) {
            for (int i = 0; i < widgets.size(); i++) {
                if (widgets.get(i).getPropertyName().equals("host_facebook") ||
                        widgets.get(i).getPropertyName().equals("host_twitter") ||
                        widgets.get(i).getPropertyName().equals("host_website")) {
                    if (isChecked) {
                        widgets.get(i).getView().setVisibility(View.VISIBLE);
                    } else {
                        widgets.get(i).getView().setVisibility(View.GONE);
                    }

                }
            }
        } else if (buttonView.getTag().toString().equals("copy_purchaser_info")) {
            String fName = "", lName = "", email = "";
            for (int i = 0; i < widgets.size(); i++) {

                switch (widgets.get(i).getPropertyName()) {
                    case "fname":
                        fName = widgets.get(i).getValue();
                        break;
                    case "lname":
                        lName = widgets.get(i).getValue();
                        break;
                    case "email":
                        email = widgets.get(i).getValue();
                        break;
                }

                if (isChecked) {
                    if (widgets.get(i).getPropertyName().contains("fname_")) {
                        widgets.get(i).setValue(fName);
                    } else if (widgets.get(i).getPropertyName().contains("lname_")){
                        widgets.get(i).setValue(lName);
                    } else if (widgets.get(i).getPropertyName().contains("email_")) {
                        widgets.get(i).setValue(email);
                    }
                } else if (widgets.get(i).getPropertyName().contains("fname_") ||
                            widgets.get(i).getPropertyName().contains("lname_") ||
                            widgets.get(i).getPropertyName().contains("email_")) {
                    widgets.get(i).setValue("");
                }
            }
        }

    }

    class ChangeHandler implements CompoundButton.OnCheckedChangeListener {
        protected FormWidget _widget;

        public ChangeHandler( FormWidget widget ) {
            _widget = widget;
        }
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
        {
            if( _handler != null ){
                _handler.toggle( _widget );
            }
        }

    }

    @Override
    public void onClick(final View v) {

        /**
         * Work to delete the friends list on Add to List page.
         *
         */
        final String list_id = v.getTag().toString();

        final String actionUrl = AppConstant.DEFAULT_URL + "user/list-delete?list_id=" + list_id +
                "&friend_id=" + BrowseMemberAdapter.sFriendId;

        mAlertDialogWithAction.showAlertDialogWithAction(mContext.getResources().getString(R.string.delete_list_title),
                mContext.getResources().getString(R.string.delete_list_dialogue_message),
                mContext.getResources().getString(R.string.delete_list_title),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        mAppConst.showProgressDialog();
                        mAppConst.deleteResponseForUrl(actionUrl, null, new OnResponseListener() {
                            @Override
                            public void onTaskCompleted(JSONObject jsonObject) {
                                mAppConst.hideProgressDialog();
                                /* Show Message */
                                SnackbarUtils.displaySnackbarLongTime(_layout,
                                        mContext.getResources().getString(R.string.successful_submit));
                                View view = _layout.findViewById(R.id.property);
                                _layout.removeView(view);

                            }

                            @Override
                            public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                                mAppConst.hideProgressDialog();
                            }
                        });
                    }
                });
    }
}
