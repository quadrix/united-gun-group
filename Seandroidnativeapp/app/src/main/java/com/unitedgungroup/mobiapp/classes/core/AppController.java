/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.core;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.Volley;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.TLSSocketFactory;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import io.fabric.sdk.android.Fabric;

public class AppController extends MultiDexApplication {

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;

    private FirebaseAnalytics mFirebaseAnalytics;

    private static AppController mInstance;

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        FontChanger.setDefaultFont(this, "MONOSPACE", getResources().getString(R.string.default_font_name));
        FontChanger.setDefaultFont(this, "DEFAULT", getResources().getString(R.string.default_font_name));
        FontChanger.setDefaultFont(this, "SERIF", getResources().getString(R.string.default_font_name));
        FontChanger.setDefaultFont(this, "SANS_SERIF", getResources().getString(R.string.default_font_name));
        mInstance = this;
        VolleyLog.DEBUG = false;

        TwitterAuthConfig authConfig =
                new TwitterAuthConfig(getResources().getString(R.string.twitter_key),
                        getResources().getString(R.string.twitter_secret));

        Fabric.with(this, new Twitter(authConfig));

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        // Updating current app version in preferences.
        try {
            PreferencesUtils.updateCurrentAppVersionPref(this, this.getPackageManager().
                    getPackageInfo(this.getPackageName(), 0).versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mRequestQueue = Volley.newRequestQueue(getApplicationContext());
            } else {
                HttpStack stack = null;
                try {
                    stack = new HurlStack(null, new TLSSocketFactory());
                } catch (KeyManagementException e) {
                    e.printStackTrace();
                    stack = new HurlStack();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                    stack = new HurlStack();
                }
                mRequestQueue = Volley.newRequestQueue(getApplicationContext(), stack);
            }
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setShouldCache(false);
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }


    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    public void trackEvent(String category, String action, String label) {

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, category);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, label);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE , action);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

    }

}