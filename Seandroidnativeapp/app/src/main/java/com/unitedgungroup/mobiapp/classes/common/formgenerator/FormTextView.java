package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.facebook.CallbackManager;
import com.facebook.login.widget.LoginButton;
import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.WebViewActivity;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.ui.SelectableTextView;
import com.unitedgungroup.mobiapp.classes.common.utils.SocialLoginUtil;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONObject;

import java.util.Arrays;

public class FormTextView extends FormWidget {

    protected SelectableTextView _label;
    protected View view;
    public static final LinearLayout.LayoutParams viewParams = CustomViews.
            getCustomWidthHeightLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1);
    public static CallbackManager callbackManager;
    public static TwitterLoginButton twitterLoginButton;

    public FormTextView(final Context context, final String property, boolean hasValidator, String label, final JSONObject jsonObject) {
        super(context, property, hasValidator);

        LinearLayout.LayoutParams layoutParams = CustomViews.getFullWidthLayoutParams();
        layoutParams.setMargins(FormActivity.defaultLayoutParams.leftMargin,
                context.getResources().getDimensionPixelSize(R.dimen.margin_5dp),
                FormActivity.defaultLayoutParams.rightMargin,
                context.getResources().getDimensionPixelSize(R.dimen.margin_5dp));
        layoutParams.gravity = Gravity.CENTER;

        // Show terms of service link and make that clickable to open it in webview.
        if( property != null && property.equals("terms_url")) {
            AppCompatTextView termsTextView = new AppCompatTextView(context);
            termsTextView.setText(label);
            termsTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));

            termsTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent termsOfServiceIntent = new Intent(context, WebViewActivity.class);
                    termsOfServiceIntent.putExtra("url",  jsonObject.optString("url"));
                    context.startActivity(termsOfServiceIntent);
                    ((Activity) context).overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                }
            });

            _layout.addView(termsTextView);

        } else if (property != null && property.equals("facebook")) {
            // Adding facebook/twitter button when its coming in response for the integration.

            /* Initialize Facebook SDK, we need to initialize before using it ---- */
            SocialLoginUtil.initializeFacebookSDK(context);
            SocialLoginUtil.clearFbTwitterInstances(context, "facebook");

            callbackManager = CallbackManager.Factory.create();
            LoginButton facebookLoginButton = new LoginButton(context);
            facebookLoginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday"));
            facebookLoginButton.setPadding(context.getResources().getDimensionPixelSize(R.dimen.margin_15dp),
                    context.getResources().getDimensionPixelSize(R.dimen.login_button_top_bottom_padding),
                    0, context.getResources().getDimensionPixelSize(R.dimen.login_button_top_bottom_padding));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                facebookLoginButton.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
            facebookLoginButton.setGravity(Gravity.CENTER);
            facebookLoginButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    context.getResources().getDimensionPixelSize(R.dimen.body_default_font_size));
            facebookLoginButton.setLayoutParams(layoutParams);
            if (!context.getResources().getString(R.string.facebook_app_id).isEmpty()) {
                facebookLoginButton.setVisibility(View.VISIBLE);
                addView(context, label);
            } else {
                facebookLoginButton.setVisibility(View.GONE);
            }
            _layout.addView(facebookLoginButton);

            //Facebook login authentication process
            SocialLoginUtil.registerFacebookLoginCallback(context, _layout, callbackManager, true);

        } else if (property != null && property.equals("twitter")) {
            /* Initialize Twitter SDK, for login using twitter */
            SocialLoginUtil.initializeTwitterSDK(context);
            SocialLoginUtil.clearFbTwitterInstances(context, "twitter");
            twitterLoginButton = new TwitterLoginButton(context);

            twitterLoginButton.setLayoutParams(layoutParams);
            twitterLoginButton.setGravity(Gravity.CENTER);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                twitterLoginButton.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
            twitterLoginButton.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                    context.getResources().getDimensionPixelSize(R.dimen.body_default_font_size));
            twitterLoginButton.setHeight(20);

            // Hide twitter button when twitter_key or twitter_secret is null or Empty.
            if (!context.getResources().getString(R.string.twitter_key).isEmpty() &&
                    !context.getResources().getString(R.string.twitter_secret).isEmpty()) {
                twitterLoginButton.setVisibility(View.VISIBLE);
                addView(context, label);
            } else {
                twitterLoginButton.setVisibility(View.GONE);
            }
            _layout.addView(twitterLoginButton);

            //Twitter login authentication process
            SocialLoginUtil.registerTwitterLoginCallback(context, _layout, twitterLoginButton, true);

        } else {
            addView(context, label);
        }
    }

    /**
     * Method to add label view.
     * @param context Context of the class.
     * @param label Label to be shown.
     */
    public void addView(Context context, String label) {
        _label = new SelectableTextView( context );
        _label.setText(label);
        _label.setTypeface(null, Typeface.BOLD);
        _label.setLayoutParams(FormActivity.defaultLayoutParams);

        view = new View(context);
        view.setLayoutParams(viewParams);

        _layout.addView(_label);
        _layout.addView(view);
    }
}
