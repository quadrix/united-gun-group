/*
 *   Copyright (c) 2016 BigStep Technologies Private Limited.
 *
 *   You may not use this file except in compliance with the
 *   SocialEngineAddOns License Agreement.
 *   You may obtain a copy of the License at:
 *   https://www.socialengineaddons.com/android-app-license
 *   The full copyright and license information is also mentioned
 *   in the LICENSE file that was distributed with this
 *   source code.
 */

package com.unitedgungroup.mobiapp.classes.core;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.unitedgungroup.mobiapp.R;
import com.unitedgungroup.mobiapp.classes.common.activities.WebViewActivity;
import com.unitedgungroup.mobiapp.classes.common.interfaces.OnResponseListener;
import com.unitedgungroup.mobiapp.classes.common.ui.CustomViews;
import com.unitedgungroup.mobiapp.classes.common.utils.GlobalFunctions;
import com.unitedgungroup.mobiapp.classes.common.utils.PreferencesUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SnackbarUtils;
import com.unitedgungroup.mobiapp.classes.common.utils.SoundUtil;
import com.unitedgungroup.mobiapp.classes.common.utils.UrlUtil;
import com.unitedgungroup.mobiapp.classes.core.startscreens.HomeScreen;
import com.unitedgungroup.mobiapp.classes.common.utils.DataStorage;
import com.unitedgungroup.mobiapp.classes.modules.advancedActivityFeeds.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextInputLayout usernameWrapper, passwordWrapper;
    private Button loginButton;
    private String EmailValue = "antonio@getleadsfast.com";
    private String passwordValue = "test10z";
    private boolean isValidatingData = false, isError = false;
    private Context mContext;
    private AppConstant mAppConst;
    private Toolbar mToolbar;
    private TextView mForgotPassword;
    private Bundle bundle;
    private String intentAction, intentType;
    private TextView errorView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().setBackgroundDrawableResource(R.drawable.first);
        mContext = this;
        mAppConst = new AppConstant(this);

        mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        CustomViews.createMarqueeTitle(this, mToolbar);

        if (getIntent() != null) {
            bundle = getIntent().getExtras();
            intentAction = getIntent().getAction();
            intentType = getIntent().getType();
        }

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                // Playing backSound effect when user tapped on back button from tool bar.
                if (PreferencesUtils.isSoundEffectEnabled(mContext)) {
                    SoundUtil.playSoundEffectOnBackPressed(mContext);
                }
            }
        });
        loginButton = (Button) findViewById(R.id.login_button);
        loginButton.setPadding(0, (int) getResources().getDimension(R.dimen.login_button_top_bottom_padding),
                0, (int) getResources().getDimension(R.dimen.login_button_top_bottom_padding));
        loginButton.setOnClickListener(this);
        usernameWrapper = (TextInputLayout) findViewById(R.id.usernameWrapper);
        passwordWrapper = (TextInputLayout) findViewById(R.id.passwordWrapper);
        mForgotPassword = (TextView) findViewById(R.id.forgot_password);
        mForgotPassword.setOnClickListener(this);
        usernameWrapper.setHint(getResources().getString(R.string.lbl_enter_email));
        passwordWrapper.setHint(getResources().getString(R.string.lbl_enter_password));
        errorView = (TextView) findViewById(R.id.error_view);
    }

    public void LoginClicked() {

        EmailValue = usernameWrapper.getEditText().getText().toString();

        passwordValue = passwordWrapper.getEditText().getText().toString();

        if (EmailValue.isEmpty()) {
            isValidatingData = false;
            loginButton.setText(getResources().getString(R.string.login_btn_name));
            usernameWrapper.setError(getResources().getString(R.string.email_address_message));

        } else if (passwordValue.isEmpty()) {
            usernameWrapper.setErrorEnabled(false);
            isValidatingData = false;
            loginButton.setText(getResources().getString(R.string.login_btn_name));
            passwordWrapper.setError(getResources().getString(R.string.password_message));

        } else {
            mAppConst.showProgressDialog();
            usernameWrapper.setErrorEnabled(false);
            passwordWrapper.setErrorEnabled(false);

            Map<String, String> params = new HashMap<>();
            params.put("email", EmailValue);
            params.put("password", passwordValue);
            params.put("ip", GlobalFunctions.getLocalIpAddress());

            mAppConst.postLoginSignUpRequest(UrlUtil.LOGIN_URL, params, new OnResponseListener() {
                @Override
                public void onTaskCompleted(JSONObject jsonObject) {
                    mAppConst.hideProgressDialog();

                    PreferencesUtils.clearSharedPreferences(mContext);
                    PreferencesUtils.clearDashboardData(mContext);
                    DataStorage.clearApplicationData(mContext);

                    final Intent intent;
                    if (bundle != null) {
                        intent = new Intent(mContext, Status.class);
                        intent.putExtras(bundle);
                        intent.setAction(intentAction);
                        intent.setType(intentType);
                    } else {
                        intent = new Intent(mContext, MainActivity.class);
                        intent.putExtra("isSetLocation", true);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    if (jsonObject.has("oauth_token")) {

                        JSONObject userDetail = jsonObject.optJSONObject("user");
                        String user_language = userDetail.optString("language");

                        String oauthToken = jsonObject.optString("oauth_token");
                        String oauth_secret = jsonObject.optString("oauth_secret");
                        PreferencesUtils.updateUserPreferences(mContext, userDetail.toString(),
                                oauth_secret, oauthToken);

                        // Save email and base64 encrypted password in SharedPreferences
                        PreferencesUtils.UpdateLoginInfoPref(mContext, EmailValue, passwordValue,
                                userDetail.optInt("user_id"));

                        /* English is coming from API instead of it's language code, It will automatically
                           work when API issue will be resolved.. */

                        if (user_language.equals("English")) {
                            user_language = "en";
                        }
                        PreferencesUtils.updateDashBoardData(mContext,
                                PreferencesUtils.CURRENT_LANGUAGE, user_language);

                        if (user_language != null) {
                            mAppConst.changeAppLocale(user_language, false);
                        }
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                    } else if (jsonObject.optString("body") != null && !jsonObject.optString("body").isEmpty()) {

                        Intent webViewIntent = new Intent(mContext, WebViewActivity.class);
                        webViewIntent.putExtra("email", EmailValue);
                        webViewIntent.putExtra("password", passwordValue);
                        webViewIntent.putExtra("isSubscription", true);
                        webViewIntent.putExtra("url", jsonObject.optString("body"));
                        startActivity(webViewIntent);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                    }
                }

                @Override
                public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                    displayError(message);
                    mAppConst.hideProgressDialog();
                }
            });
        }
    }

    /**
     * Method to show login errors
     *
     * @param message Message which needs to show on email or password field.
     */
    public void displayError(String message) {

        if (GlobalFunctions.isValidJson(message)) {
            try {
                JSONObject jsonObject = new JSONObject(message);
                String emailError = jsonObject.optString("email");
                String passError = jsonObject.optString("password");

                if (emailError != null && !emailError.isEmpty() && passError != null && !passError.isEmpty()) {
                    usernameWrapper.setError(emailError);
                    passwordWrapper.setError(passError);

                } else if (emailError != null && !emailError.isEmpty()) {
                    usernameWrapper.setError(emailError);

                } else if (passError != null && !passError.isEmpty()) {
                    passwordWrapper.setError(passError);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            errorView.setText(message);
            errorView.setVisibility(View.VISIBLE);
            isError = true;
        }

        isValidatingData = false;
        loginButton.setText(getResources().getString(R.string.login_btn_name));
    }

    @Override
    public void onBackPressed() {
        Intent loginActivity = new Intent(LoginActivity.this, HomeScreen.class);
        loginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginActivity);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                if (!isValidatingData) {
                    if (GlobalFunctions.isNetworkAvailable(mContext)) {
                        if (isError) {
                            errorView.setVisibility(View.GONE);
                        }
                        mAppConst.hideKeyboard();
                        isValidatingData = true;
                        loginButton.setText(getResources().getString(R.string.login_validation_msg) + "……");
                        LoginClicked();
                    } else {
                        SnackbarUtils.displaySnackbarLongTime(findViewById(R.id.login_main),
                                getResources().getString(R.string.network_connectivity_error));
                    }
                }
                break;

            case R.id.forgot_password:
                forgotPassword();
                break;
        }

    }

    //Forgot Password
    private void forgotPassword() {

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this,
                android.R.style.Theme_DeviceDefault_Dialog));

        alertBuilder.setMessage(getResources().getString(R.string.forgot_password_popup_message));
        alertBuilder.setTitle(getResources().getString(R.string.forgot_password_popup_title));

        final TextInputLayout inputLayout = new TextInputLayout(this);
        final AppCompatEditText input = new AppCompatEditText(this);
        input.setHint(getResources().getString(R.string.lbl_enter_email));
        input.setTextColor(ContextCompat.getColor(this, R.color.white));
        inputLayout.addView(input);
        alertBuilder.setView(inputLayout);

        alertBuilder.setPositiveButton(getResources().getString(R.string.forgot_password_popup_button),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });


        alertBuilder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();

        // used to prevent the dialog from closing when ok button is clicked (For email condition)
        Button alertDialogPositiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        alertDialogPositiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAppConst.hideKeyboard();
                mAppConst.showProgressDialog();

                String emailAddress = inputLayout.getEditText().getText().toString();

                if (emailAddress.length() > 0 && !emailAddress.trim().isEmpty()) {
                    alertDialog.dismiss();
                    HashMap<String, String> emailParams = new HashMap<>();
                    emailParams.put("email", emailAddress);

                    mAppConst.postJsonResponseForUrl(UrlUtil.FORGOT_PASSWORD_URL, emailParams, new OnResponseListener() {
                        @Override
                        public void onTaskCompleted(JSONObject jsonObject) {
                            mAppConst.hideProgressDialog();

                                /* Show Message */
                            SnackbarUtils.displaySnackbarLongTime(findViewById(R.id.login_main),
                                    getResources().getString(R.string.forgot_password_success_message));
                        }

                        @Override
                        public void onErrorInExecutingTask(String message, boolean isRetryOption) {
                            mAppConst.hideProgressDialog();
                                /* Show Message */
                            SnackbarUtils.displaySnackbarLongTime(findViewById(R.id.login_main), message);
                        }
                    });

                } else {
                    mAppConst.hideProgressDialog();
                    inputLayout.setError(getResources().getString(R.string.forgot_password_empty_email_error_message));
                }

            }
        });

    }
}
