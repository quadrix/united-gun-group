package com.unitedgungroup.mobiapp.classes.common.formgenerator;

import android.content.Context;
import android.widget.Button;

import com.unitedgungroup.mobiapp.R;


public class FormButton extends FormWidget {
    protected Button _label;

    public FormButton(Context context, String property,String label,boolean hasValidator) {
        super(context, property,hasValidator);

        _label = new Button( context );
        _label.setText(label);
        _label.setLayoutParams( FormActivity.defaultLayoutParams );

        _label.setId(R.id.label);
        _layout.addView( _label );

    }

    @Override
    public String getValue(){
        return _label.toString();
    }

    @Override
    public void setValue( String value ) {
        _label.setText( value );
    }

    @Override
    public void setHint( String value ){
        _label.setHint( value );
    }
}
